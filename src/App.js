import React from 'react';
import Tooltip from './components/tooltip';

import './App.scss';

function App() {
  return (
    <div className="App">
      <main>
        <div className="content-wrapper">
          <h2>Tooltip</h2>

          { /* Top Left */ }
          <Tooltip tooltipContent="This is a top left tootip" tooltipPosition="tootip--top-left" tooltipTriggerContent="Open tootip" />

          { /* Top Right */ }
          <Tooltip tooltipContent="This is a top right tootip" tooltipPosition="tootip--top-right" tooltipTriggerContent="Open tootip" />

          { /* Middle Left */ }
          <Tooltip tooltipContent="This is a middle left tootip" tooltipPosition="tootip--middle-left" tooltipTriggerContent="Open tootip" />

          { /* Middle Right */ }
          <Tooltip tooltipContent="This is a middle right tootip" tooltipPosition="tootip--middle-right" tooltipTriggerContent="Open tootip" />
          
          { /* Bottom Left */ }
          <Tooltip tooltipContent="This is a bottom left tootip" tooltipPosition="tootip--bottom-left" tooltipTriggerContent="Open tootip" />

          { /* Bottom Left */ }
          <Tooltip tooltipContent="This is a bottom right tootip" tooltipPosition="tootip--bottom-right" tooltipTriggerContent="Open tootip" />
        </div>
      </main>
    </div>
  );
}

export default App;
