import React from 'react';


const Tooltip = ( { tooltipContent, tooltipPosition, tooltipTriggerContent } ) => {
	//const [isShown, setIsShown] = useState(false);
	
	function addTooltip(e) {
		// Create tooltip wrapper
		let element = e.target;
		let tooltipContent = element.dataset.content;
		// console.log(tooltipContent);
		
		createTooltipWrapper(element, tooltipContent);

	}

	function removeTooltip(e) {
		removeTooltipWrapper();
	}

	function createTooltipWrapper(element, tooltipContent) {
		// Create tooltip
		let tooltipPanel = document.createElement('div');
		tooltipPanel.setAttribute('id', 'tooltip-panel');
		tooltipPanel.innerHTML += tooltipContent;
		document.body.appendChild(tooltipPanel);	

		// Get positions of selected element
		let targetLeft = null, 
			targetTop = null; //top and left of the tooltip div

		var top = element.offsetTop;
        var left = element.offsetLeft;
        var right = element.offsetLeft + element.offsetWidth;
        var bottom = element.offsetTop + element.offsetHeight;

        // console.log("top: " + top);
        // console.log("left: " + left);
        // console.log("right: " + right);
        // console.log("bottom: " + bottom);


        if ( top > tooltipPanel.offsetHeight ) { 	// top
            targetTop = top - tooltipPanel.offsetHeight;

            if( tooltipPanel.offsetWidth > window.innerWidth - right ) { // right
            	targetLeft = right - tooltipPanel.offsetWidth;
            	// console.log ('top - right');

            } else { // left
            	targetLeft = left;
            	// console.log ('top - left');
            }
        } else { // bottom
			targetTop = bottom;

        	if( tooltipPanel.offsetWidth > window.innerWidth - right ) { // right
            	targetLeft = right - tooltipPanel.offsetWidth;
            	// console.log ('bottom - right');

            } else { // left
            	targetLeft = left;
            	// console.log ('bottom - left');
            }
        }

        // Update position of the tooltip via css
        tooltipPanel.style.top = targetTop + 'px';
        tooltipPanel.style.left = targetLeft + 'px';

        

	}

	function removeTooltipWrapper() {
		// Removes an element from the document
	    var tooltipWrapper = document.getElementById('tooltip-panel');
	    if(tooltipWrapper) {
		    tooltipWrapper.remove();
		}
	}

	return (
		<div 
			className={"tooltip-wrapper " + tooltipPosition} 
			data-content={ tooltipContent }
      		onMouseEnter={(e) => addTooltip(e)}
      		onMouseLeave={(e) => removeTooltip(e)}>
          	
      		{ tooltipTriggerContent }
      
        </div>
	)
}

export default Tooltip